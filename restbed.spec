%global _default_patch_fuzz 2

%global commit a4d820dd69b37fb8daee275d20eb162054453414
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           restbed
Version:        4.8
Release:        1%{?dist}
Summary:        RESTful functionality for C++11 applications

License:        AGPLv3+
URL:            https://github.com/Corvusoft/%{name}
Source0:        %{url}/archive/%{version}.tar.gz
Source1:        https://github.com/chriskohlhoff/asio/archive/%{commit}.tar.gz#/asio-%{shortcommit}.tar.gz
Patch0:         get_authority_fix.patch

BuildRequires:  gcc-c++
BuildRequires:  cmake
BuildRequires:  catch-devel
%if 0%{?fedora}
BuildRequires:  openssl-devel
BuildRequires:  openssl-static
Requires:       openssl
%endif

%description
Restbed is a comprehensive and consistent programming model for building
applications that require seamless and secure communication over HTTP, with the
ability to model a range of business processes, designed to target mobile,
tablet, desktop and embedded production environments.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package        static
Summary:        Static library for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    static
This package contains the static library for applications that use %{name}.

%prep
%setup -q
%setup -q -D -T -a1
%patch0 -p1
sed -i -e 's/include_directories( SYSTEM /include_directories( ${INCLUDE_DIR} SYSTEM /' test/{integration,unit,regression,feature}/CMakeLists.txt
sed -i -e '/^target_link_libraries/ s#${STATIC_LIBRARY_NAME}#restbed-shared#' test/{integration,unit,regression,feature}/CMakeLists.txt
rmdir dependency/asio && ln -s ../asio-%{commit} dependency/asio

%build
%cmake -DBUILD_TESTS=ON \
%if !0%{?fedora}
       -DBUILD_SSL=OFF \
%endif
       -DCMAKE_INSTALL_LIBDIR=%{_lib}
%cmake_build

%check
cd %__cmake_builddir
ctest

%install
%cmake_install

%files
%license LICENSE
%doc README.md
%{_libdir}/librestbed.so.*

%files devel
%{_includedir}/restbed
%{_includedir}/corvusoft/
%{_libdir}/librestbed.so

%files static
%{_libdir}/librestbed.a


%changelog
* Fri Feb 18 2022 Milivoje Legenovic <milivoje.legenovic@profidata.com> - 4.8-1
- Update to upstream version
- xdev-6 build

* Tue Jul  6 2021 Levent Demirörs <levent.demiroers@profidata.com> - 4.7^20210624ged83f45-2
- Build with asio version tagged upstream

* Tue Jun 29 2021 Levent Demirörs <levent.demiroers@profidata.com> - 4.7-1ed83f45
- Update to commit ed83f454cda90a9f1dfa9e4284fa4a8383a687f0
- Add patch to disable building/linking of static libraries

* Mon Jun 28 2021 Levent Demirörs <levent.demiroers@profidata.com> - 4.7-1
- Update library to 4.7
- Update compiler to GCC 10
- Prefer cmake macros
